<body class="hold-transition skin-green layout-top-nav" style="font-size:12px">
    <div class="wrapper">

      <header class="main-header">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <a href="{{URL::to('/')}}" class="navbar-brand"><b>ระบบสนับสนุนงานสัสดี</b></a>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                <i class="fa fa-bars"></i>
              </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">รายงานการลงบัญชีทหาร <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#"><i class="fa fa-male"></i> รายงานการลงบัญชีทั้งหมด (แบบ สด.๑)</a></li>
                      <li><a href="#"><i class="fa fa-male"></i> รายงานการลงบัญชีประจำเดือน ม.๑๖</a></li>
                      <li><a href="#"><i class="fa fa-male"></i> รายงานการลงบัญชีประจำเดือน ม.๑๘</a></li>
             
                  </ul>
                </li> 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">ระบบจัดทำแบบพิมพ์ <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                     
                     <li><a href="{{ URL::to('sd2')}}"><i class="fa fa-print"></i> จัดทำ (แบบ สด.๒) ม.๑๖ ประจำปี</a></li>
                    <li><a href="{{ URL::to('sd35')}}"><i class="fa fa-print"></i> จัดทำหมายเรียกประจำปี (แบบ สด.๓๕)</a></li>
                   
                  
                  </ul>
                </li> 
              </ul>
              <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                  <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                </div>
              </form>
            </div><!-- /.navbar-collapse -->
            <!-- Navbar Right Menu -->
              <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- Messages: style can be found in dropdown.less-->
                 

                 
                  <!-- Tasks Menu -->
                 
                  
                </ul>
              </div><!-- /.navbar-custom-menu -->
          </div><!-- /.container-fluid -->
        </nav>
      </header>