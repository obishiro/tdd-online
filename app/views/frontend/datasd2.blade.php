 
 <section class="content-header" >
             
            <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ทหารกองเกินลงบัญชีทั้งหมด </span>
                  <span class="info-box-number">{{ $numall}} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
              <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-male"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ตำบล {{ $tumbon }}</span>
                  <span class="info-box-number">{{ $numrows }} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
           
           
           
           
          </div><!-- /.row -->
        </section>

 <div class="col-xs-12">
              <div class="box" style="padding:5px;">
           
                   
            <a class="btn btn-success" style="float:right;" href="{{ URL::to('print/sd2',array($year,$district)) }}" target="_blank"><i class="fa fa-print"></i> แสดงข้อมูล</a>
        

                <div class="box-body">
 
<table id="RdataSD2" class="table table-bordered">
	<thead >
		<tr class="info">
			<th>ลำดับ</th>
			<th>เลข สด 1.</th>
         			 <th>วันที่ลงบัญชี</th>
          		
		<th>IDCARD</th>
		<th>ชื่อ</th>
		<th>นามสกุล</th>
		<th>วันเกิด</th>
		<th>บ้านเลขที่</th>
		<th>หมู่</th>
		<th>ตำบล</th>
		 
		<th>บิดา</th>
		<th>นามสกุล</th>
		<th>มารดา</th>
		<th>นามสกุล</th>
          <th>เล่มที่</th>
          <th>เลข สด.9</th>
		<th>มาตรา</th>
	        
		 </tr>

		 @foreach($data as $getdata => $d) 
			<tr @if($i%2==0) class="success" @endif>
				<td>{{ $i }}</td>
				<td>{{ $d->runningno}}</td>
				<td>{{Helpers::CbirthEn($d->writtendate) }}</td>
				<td>{{ $d->identificationnumber}}</td>
				<td>นาย{{ $d->firstname}}</td>
				<td>{{ $d->lastname}}</td>
				<td>{{ Helpers::CbirthEn($d->birthdate)}}</td>
				<td>{{ $d->homeno}}</td>
				<td>{{ $d->moo}}</td>
				<td>{{ $d->distric_name}}</td>
				<td>{{ $d->fathername}}</td>
				<td>{{ $d->fatherlastname}}</td>
				<td>{{ $d->mothername}}</td>
				<td>{{ $d->motherlastname}}</td>
				<td>{{ $d->booksd9}}</td>
				<td>{{ $d->nosd9}}</td>
				<td>{{ $d->section}}</td>
			</tr>
			<?php $i++;?>
		@endforeach
	</thead>
</table>


  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            @section('script')
  <script>
$(function() {
	var year = $('#year').val();
	var district = $('#district').val();
    $('#RdataSD2').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'getdatasd2/'+year+'/'+district,
        columns: [
          {data:'writtendate',name:'writtendate',searchable:false},
          {data:'identificationnumber',name:'identificationnumber'},
          {data:'firstname',name:'firstname',searchable:true},
          {data:'lastname',name:'lastname',searchable:true},
          {data:'birthdate',name:'birthdate',searchable:true},
          {data:'homeno',name:'homeno',searchable:false},
          {data:'moo',name:'moo',searchable:false},
          {data:'distric_name',name:'distric_name',searchable:false},
          {data:'fathername',name:'fathername',searchable:true},
          {data:'fatherlastname',name:'fatherlastname',searchable:true},
          {data:'mothername',name:'mothername',searchable:true},
          {data:'motherlastname',name:'motherlastname',searchable:true},
          {data:'booksd9',name:'booksd9',searchable:true},
          {data:'nosd9',name:'nosd9',searchable:true},
          {data:'section',name:'section',searchable:false},
         
         
        ]
        
    });
});
</script>
@stop