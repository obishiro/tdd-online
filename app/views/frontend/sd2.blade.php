@extends('master')
@section('content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            ระบบจัดทำ สด.๒ ม.๑๖ ประจำปี
             
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>

        <section class="content-header">
	{{ Form::open(array(
		'url'=>'print/sd2',
		'method' =>'POST',
		'target' => 'blank'
		))}}
	<div class="row">
        	<div class="col-md-1 col-sm-6 col-xs-12">
        		<label for="">เลือกปีเกิด</label>
         		<select name="year" id="year" class="form-control">
         		<option value=""></option>
         		 {{ Helpers::ChooseYear(); }} 
         		</select>
         	</div>	
         	<div class="col-md-3 col-sm-6 col-xs-12">
        		<label for="">เลือกตำบล</label>
         		<select name="district" id="district" class="form-control">
         		<option value=""></option>
         		@foreach($tumbon as $tumbons =>$t)
         		<option value="{{ $t->distric_id }}">{{ $t->distric_name}}</option>
         		@endforeach
         		</select>
         	</div>	
         	</div>
          
         	<div class="row">
         	<div class="col-md-1 col-sm-6 col-xs-12">         		 
         		<button class="btn btn-primary" type="button" id="bt-getdata" style="margin-top:24px"><i class="fa fa-refresh"></i> ดึงข้อมูล</button>
         	</div>
         	</div>
         {{ Form::close() }}

        </section>
	 

       
        <section class="content"  style="margin-top:50px">
         <div class="row"  id="show_datasd2">
             
         </div>
       
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@stop
@section('script')
<script>
$('#bt-getdata').click(function(){
    var year = $('#year').val();
    var district = $('#district').val();
    $('#show_datasd2').load('datasd2/'+year+'/'+district,{
       ajax:true, test:$(this).val() });
    
});
 
</script>

@stop