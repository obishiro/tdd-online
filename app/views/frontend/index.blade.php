@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            รายชื่อทหารกองเกินที่ลงบัญชีทหารกองเกิน 
             <small>ทั้งหมด จำนวน</small>
              <span class="label label-danger">{{ $allnumrows }}</span> 
               <small> คน</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>
        <section class="content-header">
             
            <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">เดือน {{ Helpers::Cmonth(date('m'))}} พ.ศ. {{Helpers::cthai(date('Y')+543)}}</span>
                  <span class="info-box-number">{{ $numrows }} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
             <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-male"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ลงบัญชี ตาม ม.16 </span>
                  <span class="info-box-number">{{ $numrows16 }} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
             <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

             <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-male"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ลงบัญชี ตาม ม.18</span>
                  <span class="info-box-number">{{ $numrows18 }} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-male"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">ม.18 ที่อยู่ในกำหนดเรียก</span>
                  <span class="info-box-number">{{ $numrows18in }} คน</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

           
           
           
          </div><!-- /.row -->
        </section>


        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
           
                   
           
                <div class="box-body">
 
<table id="Rdata" class="table table-bordered table-hover table-striped">
	<thead >
		<tr>
          <th>วันที่ลงบัญชี</th>
		<th>IDCARD</th>
		<th>ชื่อ</th>
		<th>นามสกุล</th>
		<th>วันเกิด</th>
		<th>บ้านเลขที่</th>
		<th>หมู่</th>
		<th>ตำบล</th>
		 
		<th>บิดา</th>
		<th>นามสกุล</th>
		<th>มารดา</th>
		<th>นามสกุล</th>
          <th>เล่มที่</th>
          <th>เลข สด.9</th>
		<th>มาตรา</th>
	       <th>พิมพ์</th>
		 </tr>
	</thead>
</table>


  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@stop
@section('script')
<script>
$(function() {
    $('#Rdata').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'dataregistration',
        columns: [
          {data:'writtendate',name:'writtendate',searchable:false},
          {data:'identificationnumber',name:'identificationnumber'},
          {data:'firstname',name:'firstname',searchable:true},
          {data:'lastname',name:'lastname',searchable:true},
          {data:'birthdate',name:'birthdate',searchable:true},
          {data:'homeno',name:'homeno',searchable:false},
          {data:'moo',name:'moo',searchable:false},
          {data:'distric_name',name:'distric_name',searchable:false},
          {data:'fathername',name:'fathername',searchable:true},
          {data:'fatherlastname',name:'fatherlastname',searchable:true},
          {data:'mothername',name:'mothername',searchable:true},
          {data:'motherlastname',name:'motherlastname',searchable:true},
          {data:'booksd9',name:'booksd9',searchable:true},
          {data:'nosd9',name:'nosd9',searchable:true},
          {data:'section',name:'section',searchable:false},
         
          {data:'action',name:'action',orderable:false,searchable:false}
        ]
        
    });
});
</script>
@stop