@include('template.header')
<body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="../../index2.html"><b>TDD-</b>Online</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">กรอกรหัสผู้ใช้งาน และ รหัสผ่านของท่าน</p>
       	{{ Form::open(array('action' => 'HomeController@postLogin', 'method' => 'post'))}}
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="ชื่อผู้ใช้งาน">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="รหัสผ่าน">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">เข้าสู่ระบบ</button>
            </div><!-- /.col -->
          </div>
        {{ Form::close() }}

      

       

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
