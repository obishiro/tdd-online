@extends('master')
@section('content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            ระบบจัดทำหมายเรียก (แบบ สด.๓๕) ประจำปี
             
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
          </ol>
        </section>

        <section class="content-header">
	{{ Form::open(array(
		'url'=>'print/sd35',
		'method' =>'POST',
		'target' => 'blank'
		))}}
	<div class="row">
        	<div class="col-md-1 col-sm-6 col-xs-12">
        		<label for="">เลือกปีเกิด</label>
         		<select name="year" id="year" class="form-control">
         		<option value=""></option>
         		 {{ Helpers::ChooseYear(); }} 
         		</select>
         	</div>	
         	<div class="col-md-3 col-sm-6 col-xs-12">
        		<label for="">เลือกตำบล</label>
         		<select name="district" id="district" class="form-control">
         		<option value=""></option>
         		@foreach($tumbon as $tumbons =>$t)
         		<option value="{{ $t->distric_id }}">{{ $t->distric_name}}</option>
         		@endforeach
         		</select>
         	</div>	
         	</div>
         	<div class="row" style="margin-top:10px">
         		<div class="col-md-1 col-sm-6 col-xs-12">  
         		<label for="">วันที่</label>     
         		<input type="text" name="day" class="form-control" value="5">
         		</div>
         		<div class="col-md-3 col-sm-6 col-xs-12"> 
         		<label for="">สถานที่ตรวจเลือก</label>   
         		<input type="text" name="place" class="form-control" value="ที่ว่าการอำเภอสนม">
         		</div>
         		
         	</div>
         	<div class="row">
         	<div class="col-md-1 col-sm-6 col-xs-6">         		 
         		<button class="btn btn-primary" type="submit" style="margin-top:24px"><i class="fa fa-refresh"></i> ดึงข้อมูล</button>
         	</div>
            <div class="col-md-1 col-sm-6 col-xs-6">                
                <button class="btn btn-success" id="getlistsd35" type="button" style="margin-top:24px"><i class="fa fa-list"></i> ทำงบหน้าหมายเรียกฯ</button>
            </div>
         	</div>
         {{ Form::close() }}

        </section>
	 

       
        <section class="content"  style="margin-top:50px">
         
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@stop
@section('script')
<script>
// $(function() {
//     $('#Rdata').DataTable({
//         processing: true,
//         serverSide: true,
//         ajax: 'dataregistration',
//         columns: [
//           {data:'writtendate',name:'writtendate',searchable:false},
//           {data:'identificationnumber',name:'identificationnumber'},
//           {data:'firstname',name:'firstname',searchable:true},
//           {data:'lastname',name:'lastname',searchable:true},
//           {data:'birthdate',name:'birthdate',searchable:true},
//           {data:'homeno',name:'homeno',searchable:false},
//           {data:'moo',name:'moo',searchable:false},
//           {data:'distric_name',name:'distric_name',searchable:false},
//           {data:'fathername',name:'fathername',searchable:true},
//           {data:'fatherlastname',name:'fatherlastname',searchable:true},
//           {data:'mothername',name:'mothername',searchable:true},
//           {data:'motherlastname',name:'motherlastname',searchable:true},
//           {data:'booksd9',name:'booksd9',searchable:true},
//           {data:'nosd9',name:'nosd9',searchable:true},
//           {data:'section',name:'section',searchable:false},
         
//           {data:'action',name:'action',orderable:false,searchable:false}
//         ]
        
//     });
// });
$('#getlistsd35').click(function(){
var y = $('#year').val();
var t = $('#district').val();
window.location.assign('print/listsd35/'+y+'/'+t);
});
</script>
@stop