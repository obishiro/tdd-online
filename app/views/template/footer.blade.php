 <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Theme&Design  &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a> พัฒนาระบบโดย <a href="https://www.facebook.com/inw0000">สิบเอกสนธยา อุปถัมภ์ </a></strong>All rights reserved.
      </footer> 
 <!-- jQuery 2.1.4 -->
    <script src="{{ URL::to('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ URL::to('js/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{URL::to('js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <script src="{{ URL::to('js/raphael-min.js') }}"></script>
    
     <script src="{{ URL::to('plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ URL::to('plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
    @yield('script')

  </body>
</html>