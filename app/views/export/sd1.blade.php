<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<?php
			$booksd9 = Helpers::cthai($data->booksd9);
                                $nosd9 = Helpers::cthai($data->nosd9);
                                $iden = Helpers::FnID($data->identificationnumber);
                                $pid = Helpers::cthai($iden);
                                $homeno =Helpers::cthai($data->homeno);
                                $moo = Helpers::cthai($data->moo);
                                $education = Helpers::cthai($data->education);
                                $idcardno=Helpers::cthai($data->idcardno);
                                $birthdate = Helpers::Cbirth($data->birthdate);
                                $age = Helpers::Age($data->writtendate,$data->birthdate);
                                $class = Helpers::ClassY($data->birthdate);
                                $datewrite = Helpers::Datewrite($data->writtendate);
                                $monthwrite = Helpers::Monthwrite($data->writtendate);
                                $yearwrite = Helpers::Yearwrite($data->writtendate);
                                 $yod = Helpers::Yod(Input::get('yod'));
                                $army_position =Helpers::ArmyPosition(Input::get('army-position'),$data->amphur_name);
                                $amphur_position =Helpers::AmphurPosition(Input::get('amphur-position'),$data->amphur_name);
                                $amphur_position_addon =Helpers::AmphurPositionAddon(Input::get('amphur-position'),$data->amphur_name);
                                $idcardissuedate = Helpers::Cidcarddate($data->idcardissuedate);
                                ?>
<table width="100%">
<tr>
	<td width="13%"  height="195"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	 
	<td>{{ $data->distric_name}}</td>
</tr>
<tr>
	<td width="13%" height="29"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="3%"></td>
	<td  width="13%">{{ $datewrite }}</td>
	<td    colspan="3" align="center">{{ $monthwrite}}</td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	 <td  width="5%" >{{ $yearwrite }}</td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="27"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	 
	<td  width="2%" colspan="2">{{ $booksd9 }}</td>
	<td  width="7%"></td>
	<td    colspan="3" align="center">{{ $nosd9 }}</td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	 <td  width="5%">&nbsp;&nbsp;{{ $class }}</td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="42"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	 
	<td  width="2%"></td>
	<td  width="7%"></td>
	<td     ></td>
	<td  width="5%">{{ $data->firstname}} &nbsp;&nbsp;&nbsp;&nbsp;  {{ $data->lastname}}</td>
	<td  width="7%"></td>
	 <td  width="5%"></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="40"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	 
	<td  width="2%"></td>
	<td  width="7%"></td>
	<td     >&nbsp;&nbsp;{{ $pid}}</td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	 <td  width="5%"></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="22"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%">{{ $data->zodiac}}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   > </td>
	<td  >{{$birthdate}}</td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" >{{ $age }}</td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="25"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   >{{ $data->flaw }}</td>
	<td  >  </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="25"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%">{{$r->religionname}}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="30"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%">{{$data->fathername }} &nbsp;&nbsp; {{$data->fatherlastname}}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" >{{ $na->nationality_name}}</td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="26"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%">{{$data->mothername }} &nbsp;&nbsp; {{$data->motherlastname}}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%"  height="105"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%">{{$homeno}}</td>
	<td  width="7%"></td>
	<td  width="5%"></td>
	 
	<td></td>
</tr>
<tr>
	<td width="13%" height="26"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" >{{ $moo}}</td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="26"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	<td  width="5%" align="left"></td>
	<td  width="5%">{{ $data->distric_name }}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  &nbsp;&nbsp;{{$data->amphur_name}}</td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" >&nbsp;&nbsp;{{ $data->province_name}}</td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="26"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	<td  width="5%" align="center"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  >  {{ $education}}</td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="26"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	<td  width="5%" align="center"></td>
	<td  width="5%">&nbsp;&nbsp;&nbsp;{{ $data->occupation}}</td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   ></td>
	<td  > </td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="24"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	<td  width="5%" align="center"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td   colspan="3"  align="center">{{ $idcardno }}</td>
	 
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" >&nbsp;&nbsp;{{$data->idcardissueby}}</td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="18"></td>
	<td  width="5%"></td>
	<td  width="7%"></td>
	<td  width="5%" align="center"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="2%"></td>
	<td    colspan="3"  align="center">{{ $idcardissuedate }}</td>
	 
	<td  width="5%"></td>
	<td  width="5%" ></td>
	<td  width="7%" ></td>
	 <td  width="5%" ></td>
	
	<td></td>
</tr>
 <tr>
	<td width="13%" height="80"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="3%">{{ Helpers::Yod(Input::get('yod'))}}</td>
	<td  width="13%"></td>
	<td    colspan="3" align="center"></td>
	<td  width="7%"></td>
	
	 <td  width="5%" >&nbsp;&nbsp;{{$army_position }}</td>
	
	<td></td>
</tr>
 <tr>
	<td width="13%" height="24"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="3%"></td>
	<td  width="13%"></td>
	<td    colspan="3" align="center"></td>
	<td  width="7%"></td>
	
	 <td  width="5%" >&nbsp;&nbsp;{{$amphur_position }}</td>
	
	<td></td>
</tr>
<tr>
	<td width="13%" height="20"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="5%"></td>
	<td  width="3%"></td>
	<td  width="13%"></td>
	<td    colspan="3" align="center"></td>
	<td  width="7%"></td>
	
	 <td  width="5%" >&nbsp;&nbsp;{{$amphur_position_addon }}</td>
	
	<td></td>
</tr>

</table>