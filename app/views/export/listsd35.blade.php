<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<table width="100%">
	<tr>
		<td colspan="9" align="center" valign="middle">
			<h2 style="font-size:36px">หมายเรียกเข้ารับราชการทหาร (แบบ สด.๓๕)</h2>
		</td>
		 
	</tr>
	<tr>
		<td colspan="9"    align="center" valign="middle">
			<h2  style="font-size:36px">ตำบล {{ $tumbon }}</h2>
		</td>
		 
	</tr>
	 
	<tr>
		<td style="border:1px solid #000; " width="10%"  align="right" valign="middle">หมายเรียก</td>
		<td   style="border:1px solid #000; " rowspan="2" colspan="2"  align="center" valign="middle" >ชื่อ - สกุล</td>
		<td  style="border:1px solid #000; "  colspan="2"  align="center" valign="middle">ชื่อ</td>
		<td style="border:1px solid #000; " colspan="2"  align="center" valign="middle">ภูมิลำเนาทหาร</td>
		<td  style="border:1px solid #000; " align="center" valign="middle">รับหมาย</td>
		<td  style="border:1px solid #000; " rowspan="2"  align="center" valign="middle">หมายเหตุ</td>
	</tr>
	<tr>
		 <td style="border:1px solid #000; " align="right" valign="middle">ฉบับที่</td>
		 <td width="10%" ></td>
		 <td  width="10%" ></td>
		<td  style="border:1px solid #000; " width="10%"  align="center" valign="middle">บิดา</td>
		<td  style="border:1px solid #000; " width="10%"  align="center" valign="middle">มารดา</td>
		<td  style="border:1px solid #000; " width="7%"  align="center" valign="middle">เลขที่</td>
		<td  style="border:1px solid #000; " width="5%"  align="center" valign="middle">หมู่ที่</td>
		<td  style="border:1px solid #000; " width="15%"  align="center" valign="middle">วัน เดือน ปี</td>
	 
	</tr>
	@foreach($getdata as $data =>$u)
		<?php

			 
			$homeno =Helpers::cthai($u->homeno);
                    		 $moo = Helpers::cthai($u->moo);
		?>
		<tr style="border:1px solid #000000">
		<td align="right">{{ Helpers::cthai($i)}} / {{ $class }}</td>
		<td>{{ $u->firstname}}</td>
		<td>{{ $u->lastname }}</td>
		<td>{{ $u->fathername }}</td>
		<td>{{ $u->mothername }}</td>
		<td align="center" >{{ $homeno }}</td>
		<td align="center" >{{ $moo }}</td>
		<td></td>
		 
		</tr>
		<?php $i++;?>
		
		@endforeach
		<?php for($ii = 1; $ii<=10;$ii++) {  ?>
		<tr style="border:1px solid #000000">
			<td  align="right">/ {{ $class }}</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php } ?>
		<tr>
			<td style="border:1px solid #000000" colspan="8"    align="center">
				<h2 style="font-size:36px">หมายเรียกเข้ารับราชการทหาร (แบบ สด.๓๕) สำหรับ คนอายุอื่น</h2>
			</td>
		</tr>
		<?php for($iii = 1; $iii<=10;$iii++) {  ?>
		<tr style="border:1px solid #000000">
			<td  align="right">/ {{ $class }}</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="8" height="36"></td>
		</tr>
		<tr >
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>ตรวจถูกต้อง</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr >
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>พ.ต.</td>
			<td></td>
			<td></td>
		</tr>
		<tr >
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>( เจนณรงค์  ผมน้อย )</td>
			<td></td>
			<td></td>
		</tr>
		<tr >
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>สัสดีอำเภอสนม</td>
			<td></td>
			<td></td>
		</tr>

	
</table>