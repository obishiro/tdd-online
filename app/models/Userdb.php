<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
 
class Userdb extends Eloquent implements UserInterface {
	use UserTrait;
	protected $table ='RECRUIT.ms_user';
	public $timestamps = false;
}