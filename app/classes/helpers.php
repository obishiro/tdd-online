<?php
class Helpers {
    public static function cthai ($num){
    	$num1 = str_replace('-', ' ', $num);
    return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
	array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
    $num1);
}
public static function FnID($var)
{
	$srt[0] = substr($var, 0, 1);
	$srt[1] = substr($var, 1, 4);
	$srt[2] = substr($var, 5, 5);
	$srt[3] = substr($var, 10, 2);
	$srt[4] = substr($var, 12, 1);
	return $srt[0]." ".$srt[1]." ".$srt[2]." ".$srt[3]." ".$srt[4];
}



static function Cbirth($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $dd = intval($birth[2]);
	 $d = Helpers::cthai($dd);
	 $yy = Helpers::cthai($y);
	 return $d.' '.$m.' '.$yy;
}
static function Daybirth($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $dd = intval($birth[2]);
	 $d = Helpers::cthai($dd);
	 $yy = Helpers::cthai($y);
	 return $d;
}
static function Monthbirth($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::CmonthShort($birth[1]);
	 $dd = intval($birth[2]);
	 $d = Helpers::cthai($dd);
	 $yy = Helpers::cthai($y);
	 return $m;
}
static function Yearbirth($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $dd = intval($birth[2]);
	 $d = Helpers::cthai($dd);
	 $yy = Helpers::cthai($y);
	 return  $yy;
}
static function CbirthEn($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $d =  intval($birth[2]);
	 
	 return $d.' '.$m.' '.$y;
}
public static function Age($writedate,$birth){
	$w = explode('-',$writedate);

	$b = explode('-',$birth);
	$a= $w[0]-$b[0];
	return Helpers::cthai($a);
}
public static function NumAge($writedate,$birth){
	$w = explode('-',$writedate);

	$b = explode('-',$birth);
	$a= $w[0]-$b[0];
	return  $a;
}
public static function ClassNumber($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+18;
	return $c;
}

public static function ClassY($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+18;
	return Helpers::cthai($c);
}
public static function ClassYShort($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+18;
	$cc = substr($c, 2, 2);
	return Helpers::cthai($cc);
}
public static function ClassShort($birth){
	 
	$cc = substr($birth, 2, 2);
	return Helpers::cthai($cc);
}
public static function Datewrite($var) {
	$w = explode('-',$var);
	$d =explode(' ', $w[2]);
	$day =  intval($d[0]);
	return Helpers::cthai($day);
}
public static function Monthwrite($var) {
	$w = explode('-',$var);
	return Helpers::Cmonth($w[1]);
}
public static function Yearwrite($var) {
	$w = explode('-',$var);
	return Helpers::cthai($w[0]+543);
}
public static function YearRegis($var) {
	$w = explode('-',$var);
	return Helpers::cthai($w[0]+543);
}
public static function DateR($var) {
	$w = explode('-',$var);
	$d =explode(' ', $w[2]);
	$day =  intval($d[0]);
	return  $day;
}
public static function MonthR($var) {
	$w = explode('-',$var);
	return Helpers::Cmonth($w[1]);
}
public static function YearR($var) {
	$w = explode('-',$var);
	return  $w[0]+543;
}


public static function Cmonth($var)
{
	switch ($var) {
		case '01':
			return 'มกราคม';
		break;
		case '02':
			return 'กุมภาพันธ์';
		break;
		case '03':
			return 'มีนาคม';
		break;
		case '04':
			return 'เมษายน';
		break;
		case '05':
			return 'พฤษภาคม';
		break;
		case '06':
			return 'มิถุนายน';
		break;
		case '07':
			return 'กรกฎาคม';
		break;
		case '08':
			return 'สิงหาคม';
		break;
		case '09':
			return 'กันยายน';
		break;
		case '10':
			return 'ตุลาคม';
		break;
		case '11':
			return 'พฤศจิกายน';
		break;
		case '12':
			return 'ธันวาคม';
		break;
		 
}

}
public static function CmonthShort($var)
{
	switch ($var) {
		case '01':
			return 'ม.ค.';
		break;
		case '02':
			return 'ก.พ.';
		break;
		case '03':
			return 'มี.ค.';
		break;
		case '04':
			return 'เม.ย.';
		break;
		case '05':
			return 'พ.ค.';
		break;
		case '06':
			return 'มิ.ย.';
		break;
		case '07':
			return 'ก.ค.';
		break;
		case '08':
			return 'ส.ค.';
		break;
		case '09':
			return 'ก.ย.';
		break;
		case '10':
			return 'ต.ค.';
		break;
		case '11':
			return 'พ.ย.';
		break;
		case '12':
			return 'ธ.ค.';
		break;
		 
}

}
public static function Yod($var)
{
	switch ($var){
		case '0':
			return "";
		break;
		case '1':
			return "ร้อยตรี";
		break;
		case '2':
			return "ร้อยโท";
		break;
		case '3':
			return "ร้อยเอก";
		break;
		case '4':
			return "พันตรี";
		break;
		case '5':
			return "พันโท";
		break;
		case '6':
			return "พันเอก";
		break;
		 
	}
}
public static function ArmyPosition($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "สัสดีอำเภอ".$amphur;
		break;
		case '2':
			return "ผู้ช่วยสัสดีอำเภอ".$amphur;
		break;
	}
}
public static function AmphurPosition($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "นายอำเภอ".$amphur;
		break;
		case '2':
			return "ปลัดอำเภอรักษาราชการแทน";
		break;
	}
}
public static function AmphurPositionAddon($var,$amphur){
	switch ($var) {
		case '0':
			return "";
			break;
		
		case '1':
			return "";
		break;
		case '2':
			return "นายอำเภอ".$amphur;
		break;
	}
}

static function Cidcarddate($var)
{
	 $b = explode(' ',$var);
	 $b1= $b[0];
	 $birth = explode('-', $b1);
	 $y = $birth[0]+543;
	 $m = Helpers::Cmonth($birth[1]);
	 $d = Helpers::cthai(intval($birth[2]));
	 $yy = Helpers::cthai($y);
	 return $d.' '.$m.' '.$yy;
}

public static function Noon1($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+30;
	return Helpers::cthai($c);
}
public static function Noon2($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+40;
	return Helpers::cthai($c);
}
public static function Noon3($birth){
	$b = explode('-',$birth);
	$c = ($b[0]+543)+46;
	return Helpers::cthai($c);
}
public static function ChooseYear()
{
	$y = 1996;
	$ynow = date('Y');
	$ynow = $ynow -16;
	for($i=$y;$i<$ynow;$i++) {
		$yy = $i+543;
	echo "<option value=\"$i\">$yy</option>";
		}
}

}