<?php

class PrintController extends BaseController {

	public function postPrintsd1()
        {
       
                $id= Input::get('pid');
               $height = Input::get('height');

                $u = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
			 ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
                                ->join('RECRUIT.amphur a','district','=','a.amphur_id')
                                ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','nosd9','booksd9','birthdate','education'
                                    ,'occupation','flaw','idcardno','idcardissuedate','idcardissueby','zodiac'

                                    )
			 ->where('identificationnumber',$id)->first();
                            $r = Registration::join('RECRUIT.religion','religious','=','religioncd')->select('religionname')->first();
                            $na = Registration::join('RECRUIT.nationality na','fathernationality','=','na.ID')->select('nationality_name')->first();

                               
                                $name = $u->firstname.' '.$u->lastname;

                                Excel::create('บัญชีทหารกองเกิน (สด.๑) นาย'.$name,function($excel) use ($u,$name,$r,$na){

                                $excel->setTitle('บัญชีทหารกองเกิน (สด.๑) นาย'.$name);

                                $excel->sheet('sheet1',function($sheet) use ($u,$name,$r,$na){
                                    // $sheet->setOrientation('landscape');
                                     $sheet->setPageMargin(array(0, 0, 0, 0)); // Set top, right, bottom, left
                                 //    $sheet->setOrientation('landscape');
                         
                                    
                                    $sheet->setStyle(array(
                                        'font' => array(
                                        'name'      =>  'TH SarabunPSK',
                                        'size'      =>  16,
                                      // 'bold'      =>  true
                                        )
                                    ));

                              $sheet->cell('I3', function($cells) {
                                $cells->setFontSize(20);
                              });
                               $sheet->cell('I4', function($cells) {
                                $cells->setFontSize(20);
                              });
                                $sheet->cell('H5', function($cells) {
                                $cells->setFontSize(22);
                              });
                                    $sheet->cell('H16', function($cells) {
                                $cells->setFontSize(13);
                                $cells->setAlignment('center');
                              });
                               $sheet->cell('H17', function($cells) {
                                $cells->setFontSize(13);
                                $cells->setAlignment('center');
                              });



                                     $sheet->loadView('export.sd1',array('data'=>$u,'r'=>$r,'na'=>$na));
                                });


                                })->download('xls');

   

        }



public function getPrintsd9($pid)
        {
       
                $id= $pid;
               

                $u = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
             ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
                                ->join('RECRUIT.amphur a','district','=','a.amphur_id')
                                ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','nosd9','booksd9','birthdate','education'
                                    ,'occupation','flaw','idcardno','idcardissuedate','idcardissueby','zodiac','section'

                                    )
             ->where('identificationnumber',$id)->first();
                            $r = Registration::join('RECRUIT.religion','religious','=','religioncd')->select('religionname')->first();
                            $na = Registration::join('RECRUIT.nationality na','fathernationality','=','na.ID')->select('nationality_name')->first();

                                $booksd9 = Helpers::cthai($u->booksd9);
                                $nosd9 = Helpers::cthai($u->nosd9);
                                $iden = Helpers::FnID($u->identificationnumber);
                                $pid = Helpers::cthai($iden);
                                $homeno =Helpers::cthai($u->homeno);
                                $moo = Helpers::cthai($u->moo);
                                $education = Helpers::cthai($u->education);
                                $idcardno=Helpers::cthai($u->idcardno);
                                $birthdate = Helpers::Cbirth($u->birthdate);
                                $age = Helpers::Age($u->writtendate,$u->birthdate);
                                $class = Helpers::ClassY($u->birthdate);
                                $datewrite = Helpers::Datewrite($u->writtendate);
                                $monthwrite = Helpers::Monthwrite($u->writtendate);
                                $yearwrite = Helpers::Yearwrite($u->writtendate);
                                 $yod = Helpers::Yod(Input::get('yod'));
                                $army_position =Helpers::ArmyPosition(Input::get('army-position'),$u->amphur_name);
                                $amphur_position =Helpers::AmphurPosition(Input::get('amphur-position'),$u->amphur_name);
                                $amphur_position_addon =Helpers::AmphurPositionAddon(Input::get('amphur-position'),$u->amphur_name);
                                $idcardissuedate = Helpers::Cidcarddate($u->idcardissuedate);
                                $daynoon= Helpers::cthai('1');
                                $montnoon = Helpers::Cmonth('01');
                                $yearnoon1 = Helpers::Noon1($u->birthdate);
                                $yearnoon2 = Helpers::Noon2($u->birthdate);
                                $yearnoon3 = Helpers::Noon3($u->birthdate);

                                $yearnumber = Helpers::YearRegis($u->birthdate);
                                switch($u->section){
                                    case '16':
                                        $dayin = Helpers::cthai('1');
                                        $monthin = Helpers::Cmonth('01');
                                        $yearin = $class;
                                    break;
                                    case '18':
                                        $dayin = $datewrite;
                                        $monthin = $monthwrite;
                                        $yearin = $yearwrite;
                                    break;
                                }
                        
 // switch($yearnumber)
 //    {
 //        case '2541':
 //            PDF::SetMargins(33, 41, 0, true); //left,top,right  2541/2559
 //        break;
 //        case '2542':
 //            PDF::SetMargins(33,42,0,true);
 //        break;
 //    }
  PDF::SetMargins(33, 41, 0, true); //left,top,right  2541/2559

 PDF::SetFont('thsarabun', '', 15);
//PDF::SetTitle('Hello World');

PDF::AddPage();
   $html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
 <style>
    table#r2 {
        padding-top:93px;
    }
    table#r3 {
        padding-top:90px;
    }
        table#r4 {
        padding-top:3px;
    }
    table#r5 {
        padding-top:3px;
    }
    table#r6 {
        padding-top:3px;
    }
    table#r7 {
        padding-top:3px;
    }
        table#r8 {
        padding-top:3px;
    }
        table#r9 {
        padding-top:3px;
    }
        table#r10 {
        padding-top:3px;
    }
        table#r11 {
        padding-top:3px;
    }
     table#r12 {
        padding-top:93px;
    }
         table#r13 {
        padding-top:11px;
    }
    table#r14 {
        padding-top:160px;
    }
        table#r15 {
        padding-top:3px;
    }
    table#r16 {
        padding-top:3px;
    }
    table#r17 {
        padding-top:3px;
    }
 </style>
<table border="0" width="100%">
<tr>
<td  width="8%">
</td>
<td width="45%">
<font size="18px">
$pid
 </font>
</td>
<td align="left">
  
 </td>
 </tr>

 </table>
 <table border="0" id="r2" width="100%">
<tr>
<td  width="37%"  align="left">
<font size="18px">
 
</font>
</td>
<td width="13%">
 $datewrite
</td>
<td align="left"  width="25%">
  $monthwrite
 </td>
 <td>
 $yearwrite
 </td>
 </tr>

 </table>
 <table border="0" id="r3" width="100%">
<tr>
<td  width="5%">
</td>
<td width="43%">
นาย$u->firstname &nbsp;&nbsp;$u->lastname
</td>
<td align="left">
  $u->distric_name
 </td>
 </tr>

 </table>
  <table border="0" id="r4" width="100%">
<tr>
<td  width="15%">
</td>
<td width="33%">
 
</td>
<td align="left">
 
 </td>
 </tr>
 </table>
  <table border="0" id="r5" width="100%">
<tr>
<td  width="5%">
</td>
<td width="43%">
$birthdate
</td>
<td align="left">
 
 </td>
 </tr>
 </table>
 <table border="0" id="r6" width="100%">
<tr>
<td  width="15%">
</td>
<td width="35%">
$age
</td>
<td align="left">
  $u->fathername &nbsp;&nbsp; $u->fatherlastname
 </td>
 </tr>
 </table>
 <table border="0" id="r7" width="100%">
<tr>
<td  width="9%">
</td>
<td width="41%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $u->flaw
</td>
<td align="left">
  $u->mothername &nbsp;&nbsp; $u->motherlastname
 </td>
 </tr>
 </table>
  <table border="0" id="r8" width="100%">
<tr>
<td  width="28%">
</td>
<td width="29%">
$homeno
</td>
<td align="left">
  
 </td>
 </tr>
 </table>
 <table border="0" id="r9" width="100%">
<tr>
<td  width="23%">
</td>
<td width="33%">
 
</td>
<td align="left">
    
 </td>
 </tr>
 </table>
  <table border="0" id="r10" width="100%">
<tr>
<td  width="23%">
</td>
<td width="29%">
 
</td>
<td align="left">
   
 </td>
 </tr>
 </table>
  <table border="0" id="r11" width="100%">
<tr>
<td  width="23%">
</td>
<td width="29%">
 $moo
</td>
<td align="left">
   
 </td>
 </tr>
 </table>
  <table border="0" id="r12" width="100%">
<tr>
<td  width="37%"  align="left">
 
 </td>
<td width="13%">
 
</td>
<td align="left"  width="26%">
 
 </td>
 <td>
 
 </td>
 </tr>

 </table>
  <table border="0" id="r13" width="100%">
<tr>
<td  width="23%"  align="left">
 
 </td>
<td width="17%">
 
</td>
<td align="left"  width="15%">
 
 </td>
 <td   width="23%">
 
 </td>
  <td>
 
 </td>
 </tr>

 </table>
 <table border="0" id="r14" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="28%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
  <table border="0" id="r15" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 <table border="0" id="r16" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 <table border="0" id="r17" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 
 

EOF;
PDF::writeHTML($html, true, 0, true, 0);

PDF::Output('sd9.pdf');
        }

public function postPrintsd35()
{
   $tumbon = Input::get('district') ;
   $y = Input::get('year');
  $place = Input::get('place')  ;
  $day = Input::get('day');
        $getdata = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
             ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
            ->join('RECRUIT.amphur a','district','=','a.amphur_id')
            ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','runningno' 
                                  )
                                ->where('subdistrict',$tumbon)
                                ->whereRaw("to_char(birthdate,' yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') between $y and $y" )
                                ->orderBy('runningno','asc')
                                ->get();
 PDF::SetMargins(5,0, 0, true);
 PDF::SetFont('thsarabun', '',18);
 PDF::SetCellPaddings(0,0,0,0);
 PDF::SetAutoPageBreak(TRUE, 0);

//PDF::SetTitle('Hello World');
foreach($getdata as $data =>$u){
    $iden = Helpers::FnID($u->identificationnumber);
     $pid = Helpers::cthai($iden);
     $number = Helpers::cthai($u->runningno);
     $homeno =Helpers::cthai($u->homeno);
     $moo = Helpers::cthai($u->moo);
     $dday = Helpers::cthai($day);

PDF::AddPage('P','LEGAL');
   $html = <<<EOF
   <!-- EXAMPLE OF CSS STYLE -->
 <style>
    table#r2 {
        padding-top:380px;
        margin-left:100px;
        }
    table#r3 {
        padding-top:60px;
    }
        table#r4 {
        padding-top:0px;
    }
    table#r5 {
        padding-top:0px;
    }
    table#r6 {
        padding-top:0px;
    }
    table#r7 {
        padding-top:110px;
    }
        table#r8 {
        padding-top:100px;
    }
        table#r9 {
        padding-top:1820px;
    
    }
        table#r10 {
        padding-top:6px;
    }
        table#r11 {
        padding-top:50px;
      
    }
     table#r12 {
        padding-top:93px;
    }
         table#r13 {
        padding-top:11px;
    }
    table#r14 {
        padding-top:160px;
    }
        table#r15 {
        padding-top:3px;
    }
    table#r16 {
        padding-top:3px;
    }
    table#r17 {
        padding-top:3px;
    }
 </style>
<table border="0" width="100%">
<tr>
<td   width="60%">
เลขบัตรประจำตัวประชาชน 
</td> 
<td>
<font color="#ff0000">
ในวันตรวจเลือกให้นำหลักฐานไปแสดง ดังนี้
</font>
</td>
 </tr>
 <tr>
<td   >
<font style=" font-weight: bold;font-size:130%;color:#0000">
$pid
</font>
</td>
<td>
<font color="#ff0000">
๑. หมายเรียก (แบบ สด.๓๕)
</font>
</td>
 
 </tr>
 
 <tr>
<td   >
 
</td>
<td>
<font color="#ff0000">
๒. ใบสำคัญทหารกองเกิน (แบบ สด.๙)
</font>
</td>
 
   </tr>
 <tr>
<td   >
 
</td>
<td>
<font color="#ff0000">
๓. บัตรประจำตัวประชาชน
</font>
</td>
 
 </tr>
 <tr>
<td   >
 
</td>
<td>
<font color="#ff0000">
๔. วุฒิการศึกษา ตั้งแต่ ม.๖ ขึ้นไป
</font>
</td>
  </tr>
 </table>
 <table width="100%" id="r2">
    <tr>
    <td width="10%">
    </td>
        <td width="55%">
        <font color="#0000">
        $number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font color="#000">๕๙
        </font>
        </td>
        <td>
           $u->amphur_name
        </td>
    </tr>
 </table>
  <table width="100%" id="r3">
    <tr>
    <td width="25%">
    </td>
        <td width="20%">
     <font color="#000">   ๑</font>
         </td>
          <td width="33%">
        <font color="#000">    มกราคม </font>
         </td>
         <td  >
         <font color="#000">   ๕๙ </font>
         </td>
    </tr>
 </table>
 <table width="100%" id="r4" style="color:#0000">
    <tr>
    <td width="18%">
    </td>
        <td width="25%">
        นาย$u->firstname
         </td>
          <td width="39%">
            &nbsp;&nbsp;&nbsp;$u->lastname
         </td>
         <td  >
            ๒๐
         </td>
    </tr>
 </table>
  <table width="100%" id="r4"  style="color:#0000">
    <tr>
    <td width="17%">
    </td>
        <td width="25%">
        $u->fathername - $u->mothername
         </td>
          <td width="35%">
             
         </td>
         <td  >
          $homeno
         </td>
    </tr>
 </table>
 <table width="100%" id="r5">
    <tr>
    <td width="15%">
    </td>
        <td width="12%">
         <font color="#0000">
        $moo
        </font>
         </td>
          <td width="24%">
            $u->distric_name
         </td>
         <td width="28%">
            $u->amphur_name
         </td>
         <td  >
            $u->province_name
         </td>
    </tr>
 </table>
 <table width="100%" id="r6">
    <tr>
    <td width="26%">
    </td>
        <td width="27%">
        &nbsp;  $place
         </td>
          <td width="19%">
          <font color="#ff0000" >
           $dday
           </font>
         </td>
         <td width="20%">
         <font color="#ff0000"  >
         เมษายน
        </font>
         </td>
         </tr>
 </table>
 <table width="100%" id="r7">
    <tr>
    <td width="15%">
    </td>
        <td width="24%">
        <font color="#ff0000"  >
        &nbsp;&nbsp;  ๖๐
        </font>
         </td>
          
         </tr>
 </table>
  <table width="100%" id="r8">
    <tr>
    <td width="27%">
    </td>
        <td width="24%">
        <font color="#ff0000"  >
          ๐๗.๐๐
        </font>
         </td>
          
         </tr>
 </table>
    
          
  
   
   
   
   
 
 

EOF;
 
PDF::writeHTML($html, true, 0, true, 0);
}
PDF::Output('sd35.pdf');
        }
public function getPrintsd2($year,$district)
{
 $i=1;
 $getdata = Registration::join('RECRUIT.sd1','identificationnumber','=','registrationid')
            ->join('RECRUIT.district','subdistrict','=','distric_id')
            ->select("writtendate","inputend","identificationnumber","firstname"
                ,"lastname","birthdate"
                ,"homeno","moo"
                ,"distric_name"
                ,"fathername","fatherlastname","mothername","motherlastname","booksd9","nosd9","section","runningno","occupation","flaw")
     
            //->whereRaw("to_char(inputend,'mon yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') like '%$monthnow $yearnow' ")
            ->where('subdistrict',$district)
            ->where('section','16')
           ->whereRaw("to_char(birthdate,' yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') between $year and $year" )
           ->orderBy('runningno','asc')->get();
           $tumbon = DB::table('RECRUIT.district')->where('distric_id',$district)->first();
 
        
        Excel::create('แบบพิมพ์ สด.๒ ตำบล'.$tumbon->distric_name,function($excel) use ($getdata,$i,$tumbon){

        $excel->setTitle('แบบพิมพ์ สด.๒ ตำบล'.$tumbon->distric_name);

        $excel->sheet('sheet1',function($sheet) use ($getdata,$i,$tumbon){
            // $sheet->setOrientation('landscape');
             $sheet->setPageMargin(array(0.7, 0, 0.8, 0.60));
             $sheet->setOrientation('landscape');
 
            
            $sheet->setStyle(array(
                'font' => array(
                'name'      =>  'TH SarabunPSK',
                'size'      =>  14,
                'bold'      =>  true
                )
            ));
      


             $sheet->loadView('export.sd2',array('getdata'=>$getdata,'i'=>$i));
        });


        })->download('xls');
}
 public function getListsd35($y,$tumbon)
 {
           $i=1;
        $getdata = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
                             ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
                            ->join('RECRUIT.amphur a','district','=','a.amphur_id')
                            ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','runningno' ,'birthdate'
                                  )
                                ->where('subdistrict',$tumbon)
                                ->whereRaw("to_char(birthdate,' yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') between $y and $y" )
                                ->orderBy('runningno','asc')
                                ->get();
                                $tumbon = DB::table('RECRUIT.district')->where('distric_id',$tumbon)->first();
                                $yyget = ($y+543)+20;
                                $yy =Helpers::ClassShort($yyget);
                                Excel::create('รายชื่อหมายเรียก ตำบล'.$tumbon->distric_name,function($excel) use ($getdata,$i,$tumbon,$yy){

                                $excel->setTitle('รายชื่อหมายเรียก ตำบล'.$tumbon->distric_name);

                                $excel->sheet('sheet1',function($sheet) use ($getdata,$i,$tumbon,$yy){
                                    // $sheet->setOrientation('landscape');
                                     $sheet->setPageMargin(array(0.7, 0, 0.8, 0.80)); // Set top, right, bottom, left
                                 //    $sheet->setOrientation('landscape');
                         
                                    
                                    $sheet->setStyle(array(
                                        'font' => array(
                                        'name'      =>  'TH SarabunPSK',
                                        'size'      =>  16,
                                      //  'bold'      =>  true
                                        )
                                    ));
                              


                                     $sheet->loadView('export.listsd35',array('getdata'=>$getdata,'i'=>$i,'tumbon'=>$tumbon->distric_name,'class'=>$yy));
                                });


                                })->download('xls');
 }
}