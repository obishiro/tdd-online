<?php

class ApiController extends BaseController {

	public function getDataRegistration()
	{
		$monthnow = strtolower(date("M"));
		 $yearnow = date("Y");
		$data = Registration::join('RECRUIT.sd1','identificationnumber','=','registrationid')
			->join('RECRUIT.district','subdistrict','=','distric_id')
			->select(
			 [
			 	
				"writtendate","inputend","identificationnumber","firstname"
				,"lastname","birthdate"
				,"homeno","moo"
				,"distric_name"
 				,"fathername","fatherlastname","mothername","motherlastname","booksd9","nosd9","section"
			]
			)
	 
			//->whereRaw("to_char(inputend,'mon yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') like '%$monthnow $yearnow' ")
			->orderBy('inputend','desc');
			
			return Datatables::of($data)
			 
			->editColumn('writtendate',function($data){
				
				//return Helpers::DateR($data->inputend)." ".Helpers::MonthR($data->inputend)." ".Helpers::YearR($data->inputend);
				return Helpers::CbirthEn($data->writtendate);
			})
			->editColumn('birthdate',function($data){
				return Helpers::CbirthEn($data->birthdate);
			})
			->addColumn('action',function($data){
				switch($data->section){
					case '16':
					return '<div class="btn-group">
                     
	                      <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
	                        <span class="caret"></span>
	                        <span class="sr-only">Toggle Dropdown</span>
	                      </button>
	                      <ul class="dropdown-menu left" role="menu"  style=margin-left:-100px;>
	                        <li><a href="#" data-toggle="modal" data-target="#myModal-'.$data->identificationnumber.'"><i class="fa fa-print"></i>พิมพ์ สด.1</a></li>
	                        <li><a href="print/sd9/'.$data->identificationnumber.'" target="_blank"><i class="fa fa-print"></i>พิมพ์ สด.9</a></li>
	                             
	                      </ul>
                    		</div>
			<div class="modal " id="myModal-'.$data->identificationnumber.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#000000">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header ">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">ตั้งค่าการจัดพิมพ์ สด.๑</h4>
			      </div>
			      <div class="modal-body">
				<form action="print/sd1" method="post" target="_blank">
				<input type="hidden" name="pid"  value="'.$data->identificationnumber.'">
			      <div class="row">
			      	<div class="col-xs-4">
			        <label>ชั้นยศ จนท.รับลงบัญชี</label>
			        </div>
			        <div class="col-xs-2">
			         	<select class="form-control" name="yod">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">ร้อยตรี</option>
			         		<option value="2">ร้อยโท</option>
			         		<option value="3">ร้อยเอก</option>
			         		<option value="4">พันตรี</option>
			         		<option value="5">พันโท</option>
			         		<option value="6">พันเอก</option>
			         	</select>
			         	</div>
			         	<div class="col-xs-1">
			         	<label>
			         	ตำแหน่ง
			         	</label>
			         	</div>
			         	<div class="col-xs-4">
			         	<select  class="form-control" name="army-position">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">สัสดีอำเภอ</option>
			         		<option value="2">ผู้ช่วยสัสดีอำเภอ</option>
			         	</select>
			         	</div>
			         </div>
			         <div class="row" style="margin-top:10px;">
			         	<div class="col-xs-4">
			         	  <label>ผู้ลงชื่อในใบสำคัญ</label>
				</div>
			         	  <div class="col-xs-2">
			         	<select class="form-control" name="amphur-position">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">นายอำเภอ</option>
			         		<option value="2">ปลัดอำเภอ</option>
			         	 
			         	</select>
			         	</div></div>
			         	<div class="row" style="margin-top:10px">
			         	<div class="col-xs-4">
			         	<label>
			         	กำหนดความสูงของแบบพิมพ์
			         	</label>
			         	</div>
			         	<div class="col-xs-1">
			         <input type="text" name="height" class="form-control" value="58">
			         	</div>
			         	</div>
			         	<div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดฟอร์ม</button>
			        <button type="submit" class="btn btn-success">แสดงแบบพิมพ์</button>
			      </div>
			      </div>
			      

			      </form>
			    </div>
			  </div>
			</div>

                    		';
					break;
					case '18':

					$age = Helpers::NumAge($data->inputend,$data->birthdate);
					if($age >19) {

	                        			$sd35 ='<li><a href="print/sd35/'.$data->identificationnumber.'" target="_blank"><i class="fa fa-print"></i>พิมพ์ สด.35</a></li>';
	                        
					}else{
						$sd35="";
					}
					return '<div class="btn-group">
                     
	                      <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">
	                        <span class="caret"></span>
	                        <span class="sr-only">Toggle Dropdown</span>
	                      </button>
	                      <ul class="dropdown-menu left" role="menu"  style=margin-left:-100px;>
	                        <li><a href="#" data-toggle="modal" data-target="#myModal-'.$data->identificationnumber.'"><i class="fa fa-print"></i>พิมพ์ สด.1</a></li>
	                        <li><a href="print/sd9/'.$data->identificationnumber.'" target="_blank"><i class="fa fa-print"></i> พิมพ์ สด.9</a></li>'.$sd35.'
	                        
	                      </ul>
                    		</div>
			<div class="modal " id="myModal-'.$data->identificationnumber.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="color:#000000">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header ">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">ตั้งค่าการจัดพิมพ์ สด.๑</h4>
			      </div>
			      <div class="modal-body">
				<form action="print/sd1" method="post" target="_blank">
				<input type="hidden" name="pid"   value="'.$data->identificationnumber.'">
			      <div class="row">
			      	<div class="col-xs-4">
			        <label>ชั้นยศ จนท.รับลงบัญชี</label>
			        </div>
			        <div class="col-xs-2">
			         	<select class="form-control" name="yod">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">ร้อยตรี</option>
			         		<option value="2">ร้อยโท</option>
			         		<option value="3">ร้อยเอก</option>
			         		<option value="4">พันตรี</option>
			         		<option value="5">พันโท</option>
			         		<option value="6">พันเอก</option>
			         	</select>
			         	</div>
			         	<div class="col-xs-1">
			         	<label>
			         	ตำแหน่ง
			         	</label>
			         	</div>
			         	<div class="col-xs-4">
			         	<select  class="form-control" name="army-position">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">สัสดีอำเภอ</option>
			         		<option value="2">ผู้ช่วยสัสดีอำเภอ</option>
			         	</select>
			         	</div>
			         </div>
			         <div class="row" style="margin-top:10px;">
			         	<div class="col-xs-4">
			         	  <label>ผู้ลงชื่อในใบสำคัญ</label>
				</div>
			         	  <div class="col-xs-2">
			         	<select class="form-control" name="amphur-position">
			         		<option value="0">ไม่แสดง</option>
			         		<option value="1">นายอำเภอ</option>
			         		<option value="2">ปลัดอำเภอ</option>
			         	 
			         	</select>
			         	</div>
			         	 
			         	</div>
			         	<div class="row" style="margin-top:10px">
			         	<div class="col-xs-4">
			         	<label>
			         	กำหนดความสูงของแบบพิมพ์
			         	</label>
			         	</div>
			         	<div class="col-xs-1">
			         <input type="text" name="height" class="form-control" value="58">
			         	</div>
			         	</div>
			         	<div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">ปิดฟอร์ม</button>
			        <button type="submit" class="btn btn-success">แสดงแบบพิมพ์</button>
			      </div>
			      </div>
			      

			      </form>
			    </div>
			  </div>
			</div>

                    		';
					break;
				}
				
			})
			//->removeColumn('inputend')
			->setRowClass(function ($data) {
                		//return $data->section == 18 ? 'alert-danger' : 'alert-success';
				switch ($data->section) {
					case '16':
						return 'alert-success';
						break;
					
					case '18':
						$age = Helpers::NumAge($data->inputend,$data->birthdate);
						if($age > 19) {
						return 'alert-danger';
						}else{
						return 'alert-warning';		
						}
						break;
				}
            		})
			->make(true);

	}
	public function getDatasd2($year,$district) {
		$data = Registration::join('RECRUIT.sd1','identificationnumber','=','registrationid')
			->join('RECRUIT.district','subdistrict','=','distric_id')
			->select(
			 [
			 	
				"writtendate","inputend","identificationnumber","firstname"
				,"lastname","birthdate"
				,"homeno","moo"
				,"distric_name"
 				,"fathername","fatherlastname","mothername","motherlastname","booksd9","nosd9","section"
			]
			)
	 
			//->whereRaw("to_char(inputend,'mon yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') like '%$monthnow $yearnow' ")
			->where('subdistrict',$district)
                                ->whereRaw("to_char(birthdate,' yyyy', ' NLS_DATE_LANGUAGE=AMERICAN') between $year and $year" )
                                ->orderBy('runningno','asc');
		 
			
			return Datatables::of($data)
			->editColumn('writtendate',function($data){
				
				//return Helpers::DateR($data->inputend)." ".Helpers::MonthR($data->inputend)." ".Helpers::YearR($data->inputend);
				return Helpers::CbirthEn($data->writtendate);
			})
			->editColumn('birthdate',function($data){
				return Helpers::CbirthEn($data->birthdate);
			})
			->make(true);
	}
	public function postAmphur($p)
	{
		$Amphur = Amphur::where('PROVINCE_ID',$p)->orderBy('AMPHUR_NAME','asc')->get();
		// return View::make('frontend.dataamphur')->with(array(
		// 	'amphur' =>$Amphur
		// 	));
		return $p;
	}
}