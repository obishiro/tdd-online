<?php

class PrintController extends BaseController {

	public function postPrintsd1()
        {
       
                $id= Input::get('id');
               

                $u = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
			 ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
                                ->join('RECRUIT.amphur a','district','=','a.amphur_id')
                                ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','nosd9','booksd9','birthdate','education'
                                    ,'occupation','flaw','idcardno','idcardissuedate','idcardissueby','zodiac'

                                    )
			 ->where('identificationnumber',$id)->first();
                            $r = Registration::join('RECRUIT.religion','religious','=','religioncd')->select('religionname')->first();
                            $na = Registration::join('RECRUIT.nationality na','fathernationality','=','na.ID')->select('nationality_name')->first();

                                $booksd9 = Helpers::cthai($u->booksd9);
                                $nosd9 = Helpers::cthai($u->nosd9);
                                $iden = Helpers::FnID($u->identificationnumber);
                                $pid = Helpers::cthai($iden);
                                $homeno =Helpers::cthai($u->homeno);
                                $moo = Helpers::cthai($u->moo);
                                $education = Helpers::cthai($u->education);
                                $idcardno=Helpers::cthai($u->idcardno);
                                $birthdate = Helpers::Cbirth($u->birthdate);
                                $age = Helpers::Age($u->writtendate,$u->birthdate);
                                $class = Helpers::ClassY($u->birthdate);
                                $datewrite = Helpers::Datewrite($u->writtendate);
                                $monthwrite = Helpers::Monthwrite($u->writtendate);
                                $yearwrite = Helpers::Yearwrite($u->writtendate);
                                 $yod = Helpers::Yod(Input::get('yod'));
                                $army_position =Helpers::ArmyPosition(Input::get('army-position'),$u->amphur_name);
                                $amphur_position =Helpers::AmphurPosition(Input::get('amphur-position'),$u->amphur_name);
                                $amphur_position_addon =Helpers::AmphurPositionAddon(Input::get('amphur-position'),$u->amphur_name);
                                $idcardissuedate = Helpers::Cidcarddate($u->idcardissuedate);

              //  dd($data);
          /*      
     
        Excel::create('สด.44 ',function($excel) use ($data){

        $excel->setTitle('สด.44 ');

        $excel->sheet('sheet1',function($sheet) use ($data){
            // $sheet->setOrientation('landscape');
             $sheet->setPageMargin(array(0.75, 0.50, 0.50, 0.50));
             $sheet->setHeight(1, 24);
            
             
    
              
             $sheet->cell('F7', function($cell) {

                $cell->setAlignment('center');
                $cell->setFontFamily('TH SarabunPSK');
                $cell->setFontSize(16);
                });
             
            $sheet->setStyle(array(
                'font' => array(
                'name'      =>  'TH SarabunPSK',
                'size'      =>  16,
                //'bold'      =>  true
                )
            ));


             $sheet->loadView('export.sd1',array('u'=>$data));
        });


        })->download('pdf');
     */
       
 PDF::SetMargins(56, 60, 0, true);
 PDF::SetFont('freeserif', '', 15);
//PDF::SetTitle('Hello World');

PDF::AddPage();
   $html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
 <style>
    table#r2 {
        padding-top:20px;
    }
    table#r3 {
        padding-top:-1px;
    }
    table#r4 {
        padding-top:55px;
    }
    table#r5 {
        padding-top:51px;
    }
    table#r6 {
        padding-top:15px;
    }
    table#r7 {
        padding-top:20px;
    }
    table#r8 {
        padding-top:20px;
    }
    table#r9 {
        padding-top:27px;
    }
    table#r10 {
        padding-top:27px;
    }
    table#r11 {
        padding-top:325px;
    }
    table#r12 {
        padding-top:20px;
    }
    table#r13 {
        padding-top:20px;
    }
    table#r14 {
        padding-top:23px;
    }
 
 table#r15 {
        padding-top:23px;
    }
     table#r16 {
        padding-top:30px;
    }
    table#r17 {
        padding-top:-5px;
    }
        table#r18 {
        padding-top:215px;
    }
     table#r19 {
        padding-top:32px;
    }
 </style>
<table border="0" width="100%">
<tr>
<td width="45%">
</td>
<td align="left">
 $u->distric_name 
 </td>
 </tr>

 </table>
 
 <table border="0" width="100%" id="r2" >
<tr>
<td width="18%" >
</td>
<td width="18%" valign="bottom">
 $datewrite
</td>
<td align="left" width="30%"  valign="bottom">
$monthwrite
 </td>
 <td  valign="bottom">
 $yearwrite
 </td>
 </tr>
  </table>
   <table border="0" width="100%" id="r3" >
<tr>
<td width="5%" >
</td>
<td width="25%" valign="top">
<font size="20">
   
   $booksd9
 </font>
</td>
<td align="left" width="38%"  valign="top">
<font size="20">
 $nosd9
</font>
 </td>
 <td  valign="top">
 <font size="20">
 $class
 </font>
 </td>
 </tr>
  </table>
<table border="0" width="100%" id="r4" >
<tr>
<td width="23%" >
</td>
<td>
<font size="18">
$u->firstname&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$u->lastname
</font>
</td>
</tr>
</table>
<table border="0" width="100%" id="r5" >
<tr>
<td width="23%" >
</td>
<td>
<font size="22">
$pid
</font>
</td>
</tr>
</table>
<table border="0" width="100%" id="r6" >
<tr>
<td width="23%"  align="left">
$u->zodiac
</td>
<td width="43%" valign="bottom">
 $birthdate
</td>
 
 <td  valign="bottom">
 $age
 </td>
 </tr>
</table>
<table border="0" width="100%" id="r7" >
<tr>
<td width="23%" >
</td>
<td>
$u->flaw
</td>
</tr>
</table>
<table border="0" width="100%" id="r8" >
<tr>
<td width="5%" >
</td>
<td>
 
</td>
</tr>
</table>
<table border="0" width="100%" id="r9" >
<tr>
<td width="55%">
$u->fathername $u->fatherlastname
</td>
<td>
 
</td>
 </tr>
</table>
<table border="0" width="100%" id="r10" >
<tr>
<td >
$u->mothername $u->motherlastname
</td>
 </tr>
</table>
<table border="0" width="100%" id="r11" >
<tr>
<td width="27%" >
</td>
<td width="25%">
$homeno
</td>
<td >
 
</td>
</tr>
</table>
<table border="0" width="100%" id="r12" >
<tr>
<td width="28%" >
$u->trok
</td>
<td width="34%">
$u->soi
</td>
<td >
$moo
</td>
</tr>
</table>
<table border="0" width="100%" id="r13" >
<tr>
<td width="29%" >
$u->distric_name 
</td>
<td width="34%">
 
</td>
<td >
 
</td>
</tr>
</table>
<table border="0" width="100%" id="r14" >
<tr>
<td width="23%" >
</td>
<td>
$education
</td>
</tr>
</table>
<table border="0" width="100%" id="r15" >
<tr> 
<td>$u->occupation</td>
</tr>
</table>
<table border="0" width="100%" id="r16" >
<tr>
<td width="23%" >
</td>
<td  width="38%" ><font size="13">$idcardno</font></td>
<td><font size="13"></font></td>
</tr>
</table>
<table border="0" width="100%" id="r17" >
<tr> 
<td width="23%" >
</td>
<td><font size="13">$idcardissuedate</font></td>
</tr>
</table>
<table border="0" width="100%" id="r18" >
<tr> 
<td width="13%" >
</td>
<td  width="48%">$yod</td>
<td>$army_position</td>
</tr>
</table>
<table border="0" width="100%" id="r19" >
<tr> 
<td width="61%" >
</td>

<td><font size="13">$amphur_position</font></td>
</tr>
</table>
<table border="0" width="100%"  >
<tr> 
<td width="61%" >
</td>

<td><font size="13">$amphur_position_addon</font></td>
</tr>
</table>

EOF;
PDF::writeHTML($html, true, 0, true, 0);

PDF::Output('sd1.pdf');
        }



public function getPrintsd9($pid)
        {
       
                $id= $pid;
               

                $u = Registration::join('RECRUIT.sd1 s','identificationnumber','=','s.registrationid')
                              //  ->join('RECRUIT.religion re','religious','=','re.religioncd')
                              //   ->join('RECRUIT.nationality na','fathernationality','=','na.id')
             ->join('RECRUIT.district d','subdistrict','=','d.distric_id') //join ตำบล
                                ->join('RECRUIT.amphur a','district','=','a.amphur_id')
                                ->join('RECRUIT.province p','censuscity','=','p.province_cid')
                                
                                ->select(
                                    'writtendate','identificationnumber','firstname','lastname','distric_name','fathername','fatherlastname','mothername','motherlastname'
                                    ,'homeno','moo','trok','soi','amphur_name','province_name','nosd9','booksd9','birthdate','education'
                                    ,'occupation','flaw','idcardno','idcardissuedate','idcardissueby','zodiac','section'

                                    )
             ->where('identificationnumber',$id)->first();
                            $r = Registration::join('RECRUIT.religion','religious','=','religioncd')->select('religionname')->first();
                            $na = Registration::join('RECRUIT.nationality na','fathernationality','=','na.ID')->select('nationality_name')->first();

                                $booksd9 = Helpers::cthai($u->booksd9);
                                $nosd9 = Helpers::cthai($u->nosd9);
                                $iden = Helpers::FnID($u->identificationnumber);
                                $pid = Helpers::cthai($iden);
                                $homeno =Helpers::cthai($u->homeno);
                                $moo = Helpers::cthai($u->moo);
                                $education = Helpers::cthai($u->education);
                                $idcardno=Helpers::cthai($u->idcardno);
                                $birthdate = Helpers::Cbirth($u->birthdate);
                                $age = Helpers::Age($u->writtendate,$u->birthdate);
                                $class = Helpers::ClassY($u->birthdate);
                                $datewrite = Helpers::Datewrite($u->writtendate);
                                $monthwrite = Helpers::Monthwrite($u->writtendate);
                                $yearwrite = Helpers::Yearwrite($u->writtendate);
                                 $yod = Helpers::Yod(Input::get('yod'));
                                $army_position =Helpers::ArmyPosition(Input::get('army-position'),$u->amphur_name);
                                $amphur_position =Helpers::AmphurPosition(Input::get('amphur-position'),$u->amphur_name);
                                $amphur_position_addon =Helpers::AmphurPositionAddon(Input::get('amphur-position'),$u->amphur_name);
                                $idcardissuedate = Helpers::Cidcarddate($u->idcardissuedate);
                                $daynoon= Helpers::cthai('1');
                                $montnoon = Helpers::Cmonth('01');
                                $yearnoon1 = Helpers::Noon1($u->birthdate);
                                $yearnoon2 = Helpers::Noon2($u->birthdate);
                                $yearnoon3 = Helpers::Noon3($u->birthdate);
                                switch($u->section){
                                    case '16':
                                        $dayin = Helpers::cthai('1');
                                        $monthin = Helpers::Cmonth('01');
                                        $yearin = $class;
                                    break;
                                    case '18':
                                        $dayin = $datewrite;
                                        $monthin = $monthwrite;
                                        $yearin = $yearwrite;
                                    break;
                                }
                        
       
 PDF::SetMargins(33, 41, 0, true);
 PDF::SetFont('freeserif', '', 15);
//PDF::SetTitle('Hello World');

PDF::AddPage();
   $html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
 <style>
    table#r2 {
        padding-top:93px;
    }
    table#r3 {
        padding-top:90px;
    }
        table#r4 {
        padding-top:3px;
    }
    table#r5 {
        padding-top:3px;
    }
    table#r6 {
        padding-top:3px;
    }
    table#r7 {
        padding-top:3px;
    }
        table#r8 {
        padding-top:3px;
    }
        table#r9 {
        padding-top:3px;
    }
        table#r10 {
        padding-top:3px;
    }
        table#r11 {
        padding-top:3px;
    }
     table#r12 {
        padding-top:93px;
    }
         table#r13 {
        padding-top:11px;
    }
    table#r14 {
        padding-top:160px;
    }
        table#r15 {
        padding-top:3px;
    }
    table#r16 {
        padding-top:3px;
    }
    table#r17 {
        padding-top:3px;
    }
 </style>
<table border="0" width="100%">
<tr>
<td  width="8%">
</td>
<td width="45%">
<font size="18px">
$pid
 </font>
</td>
<td align="left">
  
 </td>
 </tr>

 </table>
 <table border="0" id="r2" width="100%">
<tr>
<td  width="37%"  align="left">
<font size="18px">
 
</font>
</td>
<td width="13%">
 $datewrite
</td>
<td align="left"  width="25%">
  $monthwrite
 </td>
 <td>
 $yearwrite
 </td>
 </tr>

 </table>
 <table border="0" id="r3" width="100%">
<tr>
<td  width="5%">
</td>
<td width="43%">
นาย$u->firstname &nbsp;&nbsp;$u->lastname
</td>
<td align="left">
  $u->distric_name
 </td>
 </tr>

 </table>
  <table border="0" id="r4" width="100%">
<tr>
<td  width="15%">
</td>
<td width="33%">
 
</td>
<td align="left">
 
 </td>
 </tr>
 </table>
  <table border="0" id="r5" width="100%">
<tr>
<td  width="5%">
</td>
<td width="43%">
$birthdate
</td>
<td align="left">
 
 </td>
 </tr>
 </table>
 <table border="0" id="r6" width="100%">
<tr>
<td  width="15%">
</td>
<td width="35%">
$age
</td>
<td align="left">
  $u->fathername &nbsp;&nbsp; $u->fatherlastname
 </td>
 </tr>
 </table>
 <table border="0" id="r7" width="100%">
<tr>
<td  width="9%">
</td>
<td width="41%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $u->flaw
</td>
<td align="left">
  $u->mothername &nbsp;&nbsp; $u->motherlastname
 </td>
 </tr>
 </table>
  <table border="0" id="r8" width="100%">
<tr>
<td  width="28%">
</td>
<td width="29%">
$homeno
</td>
<td align="left">
  
 </td>
 </tr>
 </table>
 <table border="0" id="r9" width="100%">
<tr>
<td  width="23%">
</td>
<td width="33%">
 
</td>
<td align="left">
    
 </td>
 </tr>
 </table>
  <table border="0" id="r10" width="100%">
<tr>
<td  width="23%">
</td>
<td width="29%">
 
</td>
<td align="left">
   
 </td>
 </tr>
 </table>
  <table border="0" id="r11" width="100%">
<tr>
<td  width="23%">
</td>
<td width="29%">
 $moo
</td>
<td align="left">
   
 </td>
 </tr>
 </table>
  <table border="0" id="r12" width="100%">
<tr>
<td  width="37%"  align="left">
 
 </td>
<td width="13%">
 
</td>
<td align="left"  width="26%">
 
 </td>
 <td>
 
 </td>
 </tr>

 </table>
  <table border="0" id="r13" width="100%">
<tr>
<td  width="23%"  align="left">
 
 </td>
<td width="17%">
 
</td>
<td align="left"  width="15%">
 
 </td>
 <td   width="23%">
 
 </td>
  <td>
 
 </td>
 </tr>

 </table>
 <table border="0" id="r14" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="28%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
  <table border="0" id="r15" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 <table border="0" id="r16" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 <table border="0" id="r17" width="100%">
<tr>
<td  width="35%"  align="left">
 
 </td>
<td width="15%">
 
</td>
<td align="left"  width="25%">
 
 </td>
 <td >
 
 </td>
 
 </tr>

 </table>
 
 

EOF;
PDF::writeHTML($html, true, 0, true, 0);

PDF::Output('sd9.pdf');
        }

}