<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::post('login', 'HomeController@postLogin');
Route::get('login', 'HomeController@getLogin');
Route::get('dataregistration','ApiController@getDataRegistration');
Route::get('getdatasd2/{year}/{district}','ApiController@getDatasd2');
Route::get('sd35','HomeController@getSd35');
Route::get('sd2','HomeController@getSd2');
Route::post('datasd2/{year}/{district}','HomeController@postDatasd2');



/*
Print report
*/

Route::post('print/sd1','PrintController@postPrintsd1');
Route::get('print/sd9/{id}','PrintController@getPrintsd9'); 
Route::post('print/sd35','PrintController@postPrintsd35'); 
Route::get('print/sd2/{year}/{district}','PrintController@getPrintsd2'); 
Route::get('print/listsd35/{year}/{district}','PrintController@getListsd35'); 